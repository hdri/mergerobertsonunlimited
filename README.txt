MergeRobertsonUnlimited

 - a generalisation of OpenCV's implementation of "Dynamic Range Improvement Through Multiple Exposures"[1]
   - "merge many LDR images with different exposures to a single HDR image"
 - instead of accepting only LDR images, this implementation also accepts HDR image(s)
   - currently the HDR image is decomposed into LDR images with simulated exposures to capture the highlights
   - TODO: it may be possible to merge the HDR image as one image, i.e. without splitting (possibly with a speedup and better quality) using integration

[1] Mark A. Robertson, Sean Borman, and Robert L. Stevenson. Dynamic range improvement through multiple exposures. In The IEEE International Conference on Image Processing (ICIP), 1999.