//
//  main.cpp
//  MergeRobertsonUnlimited
//
//  Created by a on 28/06/2018.
//  Copyright © 2018 m-1. All rights reserved.
//

#include <iostream>
#include <vector>
#include <cassert>
#include "MergeRobertsonUnlimited.hpp"

class Tester {
	cv::Mat A, B, C, D, HDR_C;
	float Atime, Btime, Ctime, Dtime, HDR_Ctime;
	
	void load() {
		A = cv::imread("frame_0.012346.png"); Atime = 0.012346;
		B = cv::imread("frame_0.037037.png"); Btime = 0.037037;
		C = cv::imread("frame_0.111111.png"); Ctime = 0.111111;
		D = cv::imread("frame_0.333333.png"); Dtime = 0.333333;
		
		HDR_C = cv::imread("out2_fixed.exr", cv::IMREAD_UNCHANGED); HDR_Ctime = 0.111111;
	}
	
	float RMSE(const cv::Mat & X, const cv::Mat & Y) {
		cv::Mat diff = (X - Y);
		cv::pow(diff, 2, diff); // ^2
		float RMSE_B = cv::sqrt(cv::sum(diff)[0]);
		float RMSE_G = cv::sqrt(cv::sum(diff)[1]);
		float RMSE_R = cv::sqrt(cv::sum(diff)[2]);
		return (RMSE_B + RMSE_G + RMSE_R) / 3 / (X.size().width * X.size().height);
	}
	
public:
	Tester() {
		load();
	}
	
	void test4EqualToRobertson() { // custom(A, B, C, D) == robertson(A, B, C, D)
		cv::Mat robertson;
		std::cout << "Merging Robertson" << std::endl;
		cv::createMergeRobertson()->process(std::vector<cv::Mat>({A, B, C, D}), robertson, std::vector<float>({Atime, Btime, Ctime, Dtime}));
		
		cv::Mat custom;
		std::cout << "Merging Custom" << std::endl;
		cv::createMergeRobertsonUnlimited()->process(std::vector<cv::Mat>({A, B, C, D}), custom, std::vector<float>({Atime, Btime, Ctime, Dtime}));

		//std::cout << "test4EqualToRobertson: RMSE = " << RMSE(robertson, custom) << std::endl;
		assert(RMSE(robertson, custom) == 0);
	}
	
	/*void test2plus2() { // custom(ABCD) == custom(custom(AB), custom(CD))
		cv::Mat custom4;
		std::cout << "Merging Robertson" << std::endl;
		cv::createMergeRobertson()->process(std::vector<cv::Mat>({A, B, C, D}), custom4, std::vector<float>({Atime, Btime, Ctime, Dtime}));
		cv::imwrite("custom4.hdr", custom4);
		
		std::cout << "Merging Custom A, B" << std::endl;
		cv::Mat customAB;
		cv::createMergeRobertsonUnlimited()->process(std::vector<cv::Mat>({A, B}), customAB, std::vector<float>({Atime, Btime}));
		cv::imwrite("customAB.hdr", customAB);
		
		std::cout << "Merging Custom C, D" << std::endl;
		cv::Mat customCD;
		cv::createMergeRobertsonUnlimited()->process(std::vector<cv::Mat>({C, D}), customCD, std::vector<float>({Ctime, Dtime}));
		cv::imwrite("customCD.hdr", customCD);

		std::cout << "Merging Custom AB, CD" << std::endl;
		cv::Mat customABCD;
		cv::createMergeRobertsonUnlimited()->process(std::vector<cv::Mat>({customAB, customCD}), customABCD, std::vector<float>({1, 1}));
		cv::imwrite("customABCD.hdr", customABCD);
		
		float err = RMSE(custom4, customABCD);
		assert(err == 0);
	}*/
	
	void testOneHDRizedLDR() { // custom(C, D) == custom(C, HDR(D))
		cv::Mat robertson;
		std::cout << "Merging Robertson" << std::endl;
		cv::createMergeRobertson()->process(std::vector<cv::Mat>({/*A, B, */C, D}), robertson, std::vector<float>({/*Atime, Btime, */Ctime, Dtime}));
		

		cv::Mat D_HDR(D.size(), CV_32FC3);
		
		for (int y = 0; y < D.size().height; ++y) {
			for (int x = 0; x < D.size().width; ++x) {
				D_HDR.at<cv::Vec3f>(y, x) = D.at<cv::Vec3b>(y, x);
				D_HDR.at<cv::Vec3f>(y, x) /= 255;
			}
		}
		cv::imwrite("D.hdr", D);
		cv::imwrite("D_HDR.hdr", D_HDR);

		cv::Mat custom;
		std::cout << "Merging Custom" << std::endl;
		cv::createMergeRobertsonUnlimited()->process(std::vector<cv::Mat>({/*A, B, */C, D_HDR}), custom, std::vector<float>({/*Atime, Btime, */Ctime, Dtime}));
		
		cv::imwrite("robertson.hdr", robertson);
		cv::imwrite("custom.hdr", custom);

		
		float err = RMSE(robertson, custom);
		std::cout << "error: " << err << std::endl;
		assert(err == 0);
	}
	
	void testHDRandLDR() { // custom(HDR_C, D)
		cv::Mat custom;
		std::cout << "Merging Custom" << std::endl;
		cv::createMergeRobertsonUnlimited()->process(std::vector<cv::Mat>({HDR_C, D}), custom, std::vector<float>({HDR_Ctime, Dtime}));
		
		cv::imwrite("custom.hdr", custom);
		
		/*
		float err = RMSE(robertson, custom);
		std::cout << "error: " << err << std::endl;
		assert(err == 0);*/
	}
};




int main(int argc, const char * argv[]) {
	Tester t;
#define str(s) #s
#define test(test_name) do {std::cout << "test: " << str(test_name) << ":" << std::endl; t.test_name(); std::cout << " PASSED" << std::endl;} while (0)
	
	test(test4EqualToRobertson);
	//test(testOneHDRizedLDR); // TODO: fix custom merge's exposure/brightness - it should not differ when changing the number of fake exposures
	test(testHDRandLDR);

#undef test
#undef str
	//t.test4EqualToRobertson();
	return 0;
}
