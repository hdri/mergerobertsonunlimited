#ifndef MergeRobertsonUnlimited_hpp
#define MergeRobertsonUnlimited_hpp

#include <opencv2/opencv.hpp>

namespace cv {
	
	class CV_EXPORTS_W MergeRobertsonUnlimited : public MergeExposures
	{
	public:
		CV_WRAP virtual void process(InputArrayOfArrays src, OutputArray dst,
									 InputArray times, InputArray response) = 0;
		CV_WRAP virtual void process(InputArrayOfArrays src, OutputArray dst, InputArray times) = 0;
	};
	
	
	CV_EXPORTS_W Ptr<MergeRobertson> createMergeRobertsonUnlimited();
	
}

#endif /* MergeRobertsonUnlimited_hpp */
